#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 21 12:31:04 2019

@author: grace ang wan yu 
email: g.ang18@imperial.ac.uk
"""

import numpy as np 
from collections import defaultdict 
from abc import ABC, abstractmethod

class MonteCarloTreeSearchNode(ABC):

    def __init__(self, state, parent=None):
        self.state = state
        self.parent = parent
        self.children = []

    @property
    @abstractmethod
    def untried_actions(self):
        """
        Returns
        -------
        list of mctspy.games.common.AbstractGameAction
        """
        pass

    @property
    @abstractmethod
    def q(self):
        pass

    @property
    @abstractmethod
    def n(self):
        pass

    @abstractmethod
    def expand(self):
        pass

    @abstractmethod
    def is_terminal_node(self):
        pass

    @abstractmethod
    def rollout(self):
        pass

    @abstractmethod
    def backpropagate(self, reward):
        pass

    def is_fully_expanded(self):
        return len(self.untried_actions) == 0

    def best_child(self, c_param=10.0):
        choices_weights = [
            (-c.q / c.n) + c_param * np.sqrt((2 * np.log(self.n) / c.n))
            for c in self.children
        ]
        return self.children[np.argmax(choices_weights)]

    def rollout_policy(self, possible_moves):
        
        return possible_moves[np.random.randint(len(possible_moves))]
    
class WorldMonteCarloTreeSearchNode(MonteCarloTreeSearchNode):

    def __init__(self, state, parent=None):
        super().__init__(state, parent)
        self._number_of_visits = 0.
        self._value = defaultdict(float)
        self._untried_actions = None

    @property
    def untried_actions(self):
        if self._untried_actions is None:
            self._untried_actions = self.state.get_legal_actions(self.state.graph)
        return self._untried_actions

    @property
    def q(self):
        #return 
        return self._value[self.state.state['nid']]

    @property
    def n(self):
        return self._number_of_visits

    def expand(self):
        action = self.untried_actions.pop()
        next_state = self.state.move(action)
        child_node = WorldMonteCarloTreeSearchNode(
            next_state, parent=self
        )
        self.children.append(child_node)
        return child_node

    def is_terminal_node(self):
        return self.state.is_search_over()

    def rollout(self):
        current_rollout_state = self.state
        while not current_rollout_state.is_search_over():
            possible_moves = current_rollout_state.get_legal_actions(self.state.graph)
            action = self.rollout_policy(possible_moves)
            current_rollout_state = current_rollout_state.move(action)
        return self.state.state['steps'] # value of the terminal root is number of steps from root

    def backpropagate(self, cost, sim_no, result):
        self._number_of_visits += 1.
        nid = self.state.state['nid']
        previous_estimate = self._value[nid] # estimate of node value from the previous iteration 
        if self.state.state['is_leaf']: 
            new_value = cost
        else: 
            current_cost, prob_rew = self.state.calc_cost
            if len(self.children) > 0: # if node has more than 1 child, choose the one that gives the best value
                vals_dict = {}
                for c in self.children: 
                    vals_dict.update(c._value)
                new_value = np.mean([previous_estimate, current_cost]) + (1-prob_rew)*vals_dict[min(vals_dict)]
            else: 
                new_value = np.mean([previous_estimate, current_cost]) + cost
        self._value[nid] = new_value
        result.append([sim_no, nid, round(new_value,2)])
        if self.parent:
            self.parent.backpropagate(previous_estimate, sim_no, result)
        return(result)


        