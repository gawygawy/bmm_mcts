#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 21 15:14:07 2019

@author: grace ang wan yu 
email: g.ang18@imperial.ac.uk
"""

class MonteCarloTreeSearch(object):

    def __init__(self, node):
        """
        MonteCarloTreeSearchNode
        Parameters
        ----------
        node : mctspy.tree.nodes.MonteCarloTreeSearchNode
        """
        self.root = node

    def best_action(self, simulations_number, result):
        """
        Parameters
        ----------
        simulations_number : int
            number of simulations performed to get the best action
        Returns
        -------
        """
        for sim in range(0, simulations_number):
            # choose node to expand            
            v = self._tree_policy()
            terminal_node_cost = v.rollout()
            result = v.backpropagate(terminal_node_cost, sim, result)
            
        # to select best child go for exploitation only
        best_choice = self.root.best_child(c_param=0.)
        return result, best_choice 

    def _tree_policy(self):
        """
        selects node to run rollout/playout for
        Returns
        -------
        """
        current_node = self.root
        while not current_node.is_terminal_node():
            if not current_node.is_fully_expanded():
                return current_node.expand()
            else:
                current_node = current_node.best_child()
        return current_node