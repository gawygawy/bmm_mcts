#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 26 21:18:31 2019

@author: grace ang wan yu 
email: g.ang18@imperial.ac.uk
"""
# adapted from https://github.com/int8/monte-carlo-tree-search.git

import os
import re
import pandas as pd
from graph_builder import *
import numpy as np
import networkx as nx


from nodes import WorldMonteCarloTreeSearchNode
from search import MonteCarloTreeSearch 
from worldState import WorldState


all_graphs = pd.read_csv("mazes/treeBuilderPlanET.csv", sep='\t')
for world in all_graphs['world'].unique():
    graph_frame = all_graphs[all_graphs.world==world]
    g = build_graph(graph_frame)
    
    # set initial state -> root node 
    current_node = [v for v, d in g.in_degree() if d == 0][0] 
    current_pos = nx.get_node_attributes(g, "node_location")[current_node]
    steps = nx.get_node_attributes(g, "steps_from_parent")[current_node]
    new_obs = nx.get_node_attributes(g, "new_observations")[current_node]
    is_leaf = nx.get_node_attributes(g, "is_leaf")[current_node]
    black_remains = nx.get_node_attributes(g, "black_remains")[current_node]
    cell_dists = nx.get_node_attributes(g, "cell_distances")[current_node]
    
    state = {"nid":current_node,
             "pos":current_pos, 
             "steps":steps, 
             "new_obs":new_obs, 
             "is_leaf":is_leaf, 
             "black_remains":black_remains, 
             "dists": cell_dists}
    
    initial_state = WorldState(state_info=state, graph=g)
    root = WorldMonteCarloTreeSearchNode(state=initial_state)
    mcts = MonteCarloTreeSearch(root)
    results, best_node = mcts.best_action(100, [])
    
    world = re.sub(r'[^\w\s]','',world)
    df=pd.DataFrame(results ,columns=['iter', 'node', 'value'])
    df.to_csv(os.path.join(world + '.csv'), sep=',')
    