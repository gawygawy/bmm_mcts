#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 21 09:45:55 2019

@author: User
"""

from abc import ABC, abstractmethod 

class AbstractWorldState(ABC):
    
    @abstractmethod
    def is_search_over(self):
        """
        boolean indicating if searh is over
        """
        pass
    
    @abstractmethod
    def move(self, action):
        """
        consumes action and returns courtyardAbstractState
        """
        pass
    
    @abstractmethod
    def get_legal_actions(self):
        pass
    

class AbstractWorldAction(ABC):
    pass
        
