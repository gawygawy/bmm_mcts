#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 21 09:27:50 2019

@author: grace ang wan yu 
email: g.ang18@imperial.ac.uk
"""
import networkx as nx
from copy import deepcopy
import random 


from common import AbstractWorldState, AbstractWorldAction

class WorldMove(AbstractWorldAction):
    def __init__(self, x_coordinate, y_coordinate, node, steps, dists, isleaf, blackRemains):
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate 
        self.nid = node
        self.steps = steps
        self.dists = dists
        self.isleaf = isleaf
        self.blackRemains = blackRemains
        
    def __repr__(self):
        return "x:{0} y:{1} nid:{2}".format(
                self.x_coordinate, 
                self.y_coordinate, 
                self.nid)
    

class WorldState(AbstractWorldState):
    
    def __init__(self, state_info, graph):
        
        self.state = state_info
        self.graph = graph
    
    @property
    def calc_cost(self):
        cell_dists = self.state['dists']
        steps_from_parent = self.state['steps']
        prob_rew = 1/self.state['black_remains']
        
        sampled_squares = random.sample(cell_dists, 1)[0]
        
        cost = steps_from_parent + prob_rew*sampled_squares
        
        return cost, prob_rew
    
    def is_search_over(self):
        return(self.state['is_leaf'])
        
    def move(self, move):
        new_state = deepcopy(self.state)
        new_state["pos"] = [move.x_coordinate, move.y_coordinate]
        new_state["nid"] = move.nid
        new_state["steps"] = move.steps
        new_state["dists"] = move.dists
        new_state["is_leaf"] = move.isleaf
        new_state["black_remains"] = move.blackRemains
                
        return WorldState(new_state, self.graph)
    
    def get_legal_actions(self, graph):
        
        node_location_dict = nx.get_node_attributes(graph, 'node_location')
        steps_from_root_dict = nx.get_node_attributes(graph, 'steps_from_parent')
        cell_distances_dict = nx.get_node_attributes(graph, 'cell_distances')
        is_leaf_dict = nx.get_node_attributes(graph, 'is_leaf')
        black_dict = nx.get_node_attributes(graph, 'black_remains')
        
        successor_nodes = list(graph.successors(self.state["nid"]))
        next_coords = [node_location_dict[k] for k in node_location_dict if k in successor_nodes] 
        next_steps = [steps_from_root_dict[k] for k in steps_from_root_dict if k in successor_nodes]
        next_cell_dists = [cell_distances_dict[k] for k in cell_distances_dict if k in successor_nodes]
        next_is_leaf = [is_leaf_dict[k] for k in is_leaf_dict if k in successor_nodes]
        next_blacks = [black_dict[k] for k in black_dict if k in successor_nodes]
        
        # inputs to move the agent 
        next_locations = [[x[0], x[1], y, s, d, l, b] for x, y, s, d, l, b in zip(next_coords, successor_nodes, \
                         next_steps, next_cell_dists, next_is_leaf, next_blacks)]
        
        return[
                WorldMove(place[0], place[1], place[2], place[3], place[4], place[5], place[6])
                for place in next_locations
        ]
        